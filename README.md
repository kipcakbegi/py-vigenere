**Vigenere şifrelemesini uyguluyabileceğimiz basit bir Python betiği.**

Kullanımı:

```
#!python

python2 py-vigenere.py [sifrelenecek_metin] [anahtar]
```


Görünüm ve kullanım açısından basitleştirmek için tablo oluşturmaya çalıştım. Üst tarafta alfabe, sol tarafta anahtarımız yer almakta. Tablo içerisindeki kırmızı ile belirgin harflerimiz şifreyi oluşturmaktadır.

![1dz1bD.png](https://bitbucket.org/repo/7Lapoj/images/186343514-1dz1bD.png)